from django.apps import AppConfig


class SerialNumberConfig(AppConfig):
    name = 'serial_number'
